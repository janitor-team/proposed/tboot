Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: tboot
Source: https://sourceforge.net/projects/tboot/
Files-Excluded:
 uml/plantuml.jar

Files:
 include/elf_defns.h
 include/lcp2.h
 include/config.h
 include/lcp_hlp.h
 include/hash.h
 include/mle.h
 include/uuid.h
 include/tb_policy.h
 include/lcp3_hlp.h
 include/lcp3.h
 include/tboot.h
 include/lcp.h
 include/tb_error.h
 deprecated/lcptools/lcptools.h
 deprecated/lcptools/lcputils.h
 deprecated/lcptools/lcptools.c
 deprecated/lcptools/writepol.c
 deprecated/lcptools/lcputils.c
 deprecated/lcptools/relindex.c
 deprecated/lcptools/lock.c
 deprecated/lcptools/defindex.c
 deprecated/lcptools/readpol.c
 deprecated/lcptools/getcap.c
 lcptools-v2/crtpol.c
 lcptools-v2/mle_elt.c
 lcptools-v2/lcputils.h
 lcptools-v2/polelt.h
 lcptools-v2/poldata.c
 lcptools-v2/pollist2.c
 lcptools-v2/hash.c
 lcptools-v2/lcputils.c
 lcptools-v2/pollist1.c
 lcptools-v2/sbios_elt.c
 lcptools-v2/mlehash.c
 lcptools-v2/custom_elt.c
 lcptools-v2/pol.c
 lcptools-v2/pol.h
 lcptools-v2/stm_elt.c
 lcptools-v2/polelt_plugin.h
 lcptools-v2/pollist1.h
 lcptools-v2/pollist2.h
 lcptools-v2/poldata.h
 lcptools-v2/crtpollist.c
 lcptools-v2/polelt.c
 lcptools-v2/crtpolelt.c
 lcptools-v2/mle_elt_legacy.c
 lcptools-v2/pconf_legacy.c
 lcptools-v2/pollist2_1.c
 lcptools-v2/pollist2_1.h
 tboot/txt/acmod.c
 tboot/txt/txt.c
 tboot/txt/heap.c
 tboot/txt/vmcs.c
 tboot/txt/verify.c
 tboot/txt/errors.c
 tboot/txt/mtrrs.c
 tboot/include/txt/txt.h
 tboot/include/txt/config_regs.h
 tboot/include/txt/acmod.h
 tboot/include/txt/errorcode.h
 tboot/include/txt/heap.h
 tboot/include/txt/smx.h
 tboot/include/txt/vmcs.h
 tboot/include/txt/mtrrs.h
 tboot/include/txt/verify.h
 tboot/include/sha1.h
 tboot/include/cmdline.h
 tboot/include/io.h
 tboot/include/msr.h
 tboot/include/page.h
 tboot/include/processor.h
 tboot/include/loader.h
 tboot/include/string.h
 tboot/include/compiler.h
 tboot/include/tpm_20.h
 tboot/include/linux_defns.h
 tboot/include/e820.h
 tboot/include/paging.h
 tboot/include/com.h
 tboot/include/ctype.h
 tboot/include/tpm.h
 tboot/include/mutex.h
 tboot/include/multiboot.h
 tboot/include/integrity.h
 tboot/include/printk.h
 tboot/include/types.h
 tboot/include/misc.h
 tboot/include/efi_memmap.h
 tboot/include/vga.h
 tboot/include/vtd.h
 tboot/include/memlog.h
 tboot/common/memcmp.c
 tboot/common/acpi.c
 tboot/common/boot.S
 tboot/common/shutdown.S
 tboot/common/strlen.c
 tboot/common/tpm_20.c
 tboot/common/tboot.c
 tboot/common/vtd.c
 tboot/common/printk.c
 tboot/common/strtoul.c
 tboot/common/wakeup.S
 tboot/common/misc.c
 tboot/common/sha1.c
 tboot/common/linux.c
 tboot/common/hash.c
 tboot/common/strncmp.c
 tboot/common/tpm.c
 tboot/common/memlog.c
 tboot/common/loader.c
 tboot/common/strncpy.c
 tboot/common/vsprintf.c
 tboot/common/paging.c
 tboot/common/integrity.c
 tboot/common/mutex.S
 tboot/common/strcmp.c
 tboot/common/cmdline.c
 tboot/common/vga.c
 tboot/common/policy.c
 tboot/common/tb_error.c
 tboot/common/elf.c
 tboot/common/index.c
 tboot/common/efi_memmap.c
 tboot/common/tpm_12.c
 tboot/common/e820.c
 tboot/common/memcpy.c
 tb_polgen/commands.c
 tb_polgen/hash.c
 tb_polgen/param.c
 tb_polgen/tb_polgen.c
 tb_polgen/policy.c
 tb_polgen/tb_polgen.h
 txt-test/txt-test.c
 utils/txt-stat.c
 utils/txt-acminfo.c
 utils/txt-parse_err.c
Copyright:
 1989, 1990, 1991, 1992, 1993 The Regents of the University of California
 1995, 1996, 1997, 1998 WIDE Project.
 2001-2020 Intel Corporation
 2004 Artur Grabowski <art@openbsd.org>
 2016 Real-Time Systems GmbH
 2020 Cisco Systems, Inc. <pmoore2@cisco.com>
 2007, 2011, 2012, 2013, 2014, 2015, 2018 Gang Wei
 2007, 2008, 2009, 2010, 2011 Joseph Cihula
 2019, 2020 Lukasz Hawrylko
 2014, 2015, 2016, 2017, 2018 Ning Sun
 2014 Qiaowei Ren
 2008, 2009, 2010 Shane Wang
License: BSD-3-clause

Files:
 tboot/common/pci_cfgreg.c
 tboot/common/com.c
 tboot/include/atomic.h
 tboot/include/pci_cfgreg.h
 lcptools-v2/pconf2_elt.c
Copyright:
 1997 Stefan Esser <se@freebsd.org>
 1998 Doug Rabson
 1998 Michael Smith <msmith@freebsd.org>
 2000 Michael Smith <msmith@freebsd.org>
 2000 BSDi
 2004 Scott Long <scottl@freebsd.org>
 2010 Intel Corporation
 2020 Cisco Systems, Inc. <pmoore2@cisco.com>
 2007, 2009, 2010 Joseph Cihula
 2010 Shane Wang
 2014, 2018 Gang Wei
 2020 Lukasz Hawrylko
License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Files:
 safestringlib/*
Copyright:
 2008 Bo Berry
 2005-2013 by Cisco Systems, Inc.
 2012 Jonathan Toppins <jtoppins@users.sourceforge.net>
 2014-2016 Intel Corporation.
 2018 Gang Wei
License: Expat

Files:
 tboot/include/vga/ssfn.h
Copyright:
 2020 bzt (bztsrc@gitlab)
License: Expat

Files:
 tboot/include/vga/font.h
 tboot/include/vga/u_vga16.sfn
 debian/missing-sources/tboot/include/vga/u_vga16.bdf
Copyright:
 2000 Dmitry Bolkhovityanov
License: X11
 Permission is hereby granted, free of charge, to any person obtaining a copy of
 this software and associated documentation files (the "Software"), to deal in
 the Software without restriction, including without limitation the rights to
 use, copy, modify, merge, publish, distribute, and/or sell copies of the
 Software, and to permit persons to whom the Software is furnished to do so,
 provided that the above copyright notice(s) and this permission notice appear
 in all copies of the Software and that both the above copyright notice(s) and
 this permission notice appear in supporting documentation.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT OF THIRD PARTY RIGHTS.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR HOLDERS INCLUDED IN THIS NOTICE BE
 LIABLE FOR ANY CLAIM, OR ANY SPECIAL INDIRECT OR CONSEQUENTIAL DAMAGES, OR ANY
 DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
 CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 .
 Except as contained in this notice, the name of a copyright holder shall not be
 used in advertising or otherwise to promote the sale, use or other dealings in
 this Software without prior written authorization of the copyright holder.

Files:
 CHANGELOG
 deprecated/man/lcp_crtpolelt.8
 deprecated/man/lcp_writepol.8
 deprecated/man/lcp_readpol.8
 deprecated/man/lcp_mlehash.8
 deprecated/man/lcp_crtpollist.8
 deprecated/man/lcp_crtpol2.8
 deprecated/man/lcp_crtpol.8
 deprecated/man/lcp_crtpconf.8
 docs/man/tb_polgen.8
 docs/man/txt-stat.8
 docs/man/lcp2_crtpol.8
 docs/man/lcp2_crtpolelt.8
 docs/man/lcp2_crtpollist.8
 docs/man/lcp2_mlehash.8
 docs/howto_use.md
 docs/tboot_flow.md
 deprecated/man/tpmnv_defindex.8
 deprecated/man/tpmnv_getcap.8
 deprecated/man/tpmnv_lock.8
 deprecated/man/tpmnv_relindex.8
 docs/man/txt-acminfo.8
 docs/man/txt-parse_err.8
 docs/txt-info.txt
 docs/vlp.txt
 docs/policy_v1.txt
 docs/policy_v2.txt
 lcp-gen2/UserGuide.txt
 lcp-gen2/asn1spec.py
 lcp-gen2/ElementBase.py
 lcptools-v2/lcptools.txt
 README.md
 deprecated/README
 tboot/common/tboot.lds.x
 test-patches/vsprintf-test.patch
 test-patches/tpm-test.patch
 test-patches/e820-test.patch
 test-patches/mtrrs-test.patch
 txt-test/Kbuild
 Config.mk
 COPYING
 docs/Makefile
 lcp-gen2/build.py
 lcp-gen2/sbios.py
 lcp-gen2/pconf.py
 lcp-gen2/mle.py
 lcp-gen2/ElementGui.py
 lcp-gen2/util.py
 lcp-gen2/tools.py
 lcp-gen2/pdef.py
 lcp-gen2/sbiosLegacy.py
 lcp-gen2/mleLegacy.py
 lcp-gen2/list.py
 lcp-gen2/pconfLegacy.py
 lcp-gen2/defines.py
 lcp-gen2/TxtPolicyGen2.py
 lcp-gen2/stm.py
 deprecated/lcptools/Linux_LCP_Tools_User_Manual.doc
 deprecated/lcptools/Makefile
 deprecated/lcptools/Linux_LCP_Tools_User_Manual.pdf
 lcptools-v2/Makefile
 Makefile
 Doxyfile
 tboot/Config.mk
 tboot/Makefile
 tb_polgen/Makefile
 txt-test/Makefile
 utils/Makefile
 lcp-gen2/LcpPolicy.py
 .hg_archival.txt
 .hgignore
 .hgtags
 uml/launch.plantuml
 uml/load_sinit.plantuml
 uml/prepare_cpu.plantuml
 uml/shutdown.plantuml
Copyright:
 2001-2020 Intel Corporation
 2007, 2008, 2009, 2010, 2011 Joseph Cihula
 2008, 2011, 2012, 2013, 2014, 2018 Gang Wei
 2009, 2010 Shane Wang
 2014 Qiaowei Ren
 2015, 2016, 2017, 2018 Ning Sun
 2019, 2020 Lukasz Hawrylko
License: BSD-3-clause

Files:
 tboot/include/sha2.h
 tboot/common/sha256.c
 tboot/common/sha384.c
 tboot/common/sha512.c
Copyright:
 2000 Tom St Denis
License: public-domain
 The library is free for all purposes without any express
 guarantee it works.

Files:
 tboot/20_linux_tboot
 tboot/20_linux_xen_tboot
Copyright:
 2006, 2007, 2008, 2009, 2010 Free Software Foundation, Inc.
 2012, 2013, 2014, 2018 Gang Wei
 2019, 2020 Lukasz Hawrylko
 2015, 2016, 2017 Ning Sun
License: GPL-3+
 On Debian systems the full text of the GNU General Public
 License can be found in the `/usr/share/common-licenses/GPL-3'
 file.

Files:
 tboot/include/lz.h
 tboot/common/lz.c
Copyright:
 2003-2010 Marcus Geelnard
 2015, 2016 Ning Sun
License: Zlib
 This software is provided 'as-is', without any express or implied
 warranty. In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would
    be appreciated but is not required.
 .
 2. Altered source versions must be plainly marked as such, and must not
    be misrepresented as being the original software.
 .
 3. This notice may not be removed or altered from any source
    distribution.

Files:
 tboot/include/acpi.h
Copyright:
 2005 Thorsten Lockert <tholo@sigmasoft.com>
 2005 Marco Peereboom <marco@openbsd.org>
 2010 Intel Corporation
 2014 Gang Wei
 2007, 2008, 2009, 2010 Joseph Cihula
 2019 Lukasz Hawrylko
 2016, 2017 Ning Sun
 2008 Shane Wang
License: ISC
 Permission to use, copy, modify, and distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

Files:
 tboot/common/poly1305/poly1305-x86.pl
 tboot/common/poly1305/poly1305.c
 tboot/common/poly1305/x86asm.pl
 tboot/common/poly1305/x86cpuid.pl
 tboot/common/poly1305/x86gas.pl
 tboot/include/poly1305.h
Copyright:
 2007-2016 The OpenSSL Project Authors
License: Apache-2.0
 Licensed under the Apache License 2.0 (the "License").  You may not use
 this file except in compliance with the License.  You can obtain a copy
 in the file LICENSE in the source distribution or at
 https://www.openssl.org/source/license.html
 .
 On Debian systems the full text of the Apache 2.0 license can be
 found in the `/usr/share/common-licenses/Apache-2.0` file.

Files: debian/*
Copyright: 2020, 2021 Timo Lindfors <timo.lindfors@iki.fi>
License: BSD-3-clause

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 .
 Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 .
 Neither the name Intel Corporation nor the names of its contributors may be
 used to endorse or promote products derived from this software without
 specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
